Detail of the values calculated by the CPM algorithm described in the following paper: https://arxiv.org/abs/2110.01213

Code to compute the k-cliques communities: https://gitlab.lip6.fr/baudin/cpm-cpmz

Lines that start with a "#" are commented, and do not appear in the figures of the article.